# NitefulPlayground

Thanks for you interest in Niteful. Here is the deal: the language we are using to build Niteful is barely a few years old. This is exciting because it means you get to use a really excting new way to build apps. However, it makes it hard for us because, inherently, no one has more than a year of experience with this. Working on Niteful is going to require teaching yourself and learning as you go. If you are up for that, this is for you.

So, to be as subjective as possible, we are going to get as hands on as possible. You are going to get your own personal copy of a simplified app and asked to make a change based off of data and user behavior. (Which is exactly what you will be doing in your position).

## Getting Started

For help getting started with Flutter, check out Flutter’s website. [Click here.](https://flutter.io/)

It has detailed download and setup instructions.

Or instead, follow Google’s Codelab: [Click Here.](https://codelabs.developers.google.com/codelabs/first-flutter-app-pt1)

You will know that you set up everything is setup when you run “flutter doctor” and you just see checkmarks.

## Your challenge, if you choose to accept it.

_What’s this Niteful thing? That’s what users will wonder when they download Niteful. Your task is to_

_(a) introduce users to Niteful_

_(b) explain what Niteful is and how it works_

_(c) get the personal data you need from the user to use the app [Wake up time, sleep time, name]._

You can add as many scenes as you want. You can add as many third party libraries as you want. You can ask Jack as many questons as you want.

Oh and, if you have any other features you would like to add that doesn’t fit this prompt to show something cool you can do / found, add it!

## “Grading”

Jack will take notes on the quality of the code you write and quanitity of code you write. Your app, and the notes Jack took, will be passed on to the rest of the team to decide whether or not you will be the position. Make sure to mention cool things you discovered, or questions that you answered for youself by lookng it up, to Jack along the way as you code.

Right now, there isn’t a due date. If that changes, you will be notified and have a 10 day heads up.

## Tips

Make your progress easy to follow.
- Give your variables sane variable names.
- Write clear commit messages (if you don’t know what this is, ask Jack).
- Talk with Jack about your progress and let him know when/if you get stuck.

Keep in mind how you use apps.
- What do you do if you see a long screen of instructions? You skip it.
- Look at how other apps introduce you to features and concepts.
- Make it look good and make it look like it fits with the way the rest of Niteful looks.

## Submission

Notify Jack when you are done. We’ll review what you have done and let you know if you get the position.

*Note:* By submitting, you are giving Niteful permission to use your code for commercial purposes, even if you don’t get the position.
^^ sorry we have to do this, its just covering our ass.